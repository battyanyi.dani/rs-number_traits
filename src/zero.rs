use core::num::Wrapping;

pub trait Zero {
    fn zero() -> Self;
    fn is_zero(&self) -> bool;
}

macro_rules! impl_zero {
    ($($T:ident),+) => (
        $(
            impl Zero for $T {
                #[inline(always)]
                fn zero() -> Self { 0 }
                #[inline]
                fn is_zero(&self) -> bool {
                    self == &0
                }
            }
        )+
    );
}

macro_rules! impl_zero_float {
    ($($T:ident),+) => (
        $(
            impl Zero for $T {
                #[inline(always)]
                fn zero() -> Self { 0.0 }
                #[inline]
                fn is_zero(&self) -> bool {
                    self == &0.0
                }
            }
        )+
    );
}

impl_zero!(u8, u16, u32, u64, usize, i8, i16, i32, i64, isize);
impl_zero!(u128, i128);
impl_zero_float!(f32, f64);

impl<T> Zero for Wrapping<T>
where
    T: Zero,
{
    #[inline]
    fn zero() -> Self {
        Wrapping(T::zero())
    }
    #[inline]
    fn is_zero(&self) -> bool {
        self.0.is_zero()
    }
}
