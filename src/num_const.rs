use core::{f32, f64};
use core::num::Wrapping;

#[allow(non_snake_case)]
pub trait NumConst {
    fn E() -> Self;
    fn FRAC_1_PI() -> Self;
    fn FRAC_1_SQRT_2() -> Self;
    fn FRAC_2_PI() -> Self;
    fn FRAC_2_SQRT_PI() -> Self;
    fn FRAC_PI_2() -> Self;
    fn FRAC_PI_3() -> Self;
    fn FRAC_PI_4() -> Self;
    fn FRAC_PI_6() -> Self;
    fn FRAC_PI_8() -> Self;
    fn LN_10() -> Self;
    fn LN_2() -> Self;
    fn LOG10_E() -> Self;
    fn LOG2_E() -> Self;
    fn PI() -> Self;
    fn SQRT_2() -> Self;
}

macro_rules! trait_const_fixed {
    ($f:ident, $t:ident) => (
        impl NumConst for $t {
            #[inline(always)]
            fn E() -> Self { $f::consts::E as $t }
            #[inline(always)]
            fn FRAC_1_PI() -> Self { $f::consts::FRAC_1_PI as $t  }
            #[inline(always)]
            fn FRAC_1_SQRT_2() -> Self { $f::consts::FRAC_1_SQRT_2 as $t  }
            #[inline(always)]
            fn FRAC_2_PI() -> Self { $f::consts::FRAC_2_PI as $t  }
            #[inline(always)]
            fn FRAC_2_SQRT_PI() -> Self { $f::consts::FRAC_2_SQRT_PI as $t  }
            #[inline(always)]
            fn FRAC_PI_2() -> Self { $f::consts::FRAC_PI_2 as $t  }
            #[inline(always)]
            fn FRAC_PI_3() -> Self { $f::consts::FRAC_PI_3 as $t  }
            #[inline(always)]
            fn FRAC_PI_4() -> Self { $f::consts::FRAC_PI_4 as $t  }
            #[inline(always)]
            fn FRAC_PI_6() -> Self { $f::consts::FRAC_PI_6 as $t  }
            #[inline(always)]
            fn FRAC_PI_8() -> Self { $f::consts::FRAC_PI_8 as $t  }
            #[inline(always)]
            fn LN_10() -> Self { $f::consts::LN_10 as $t  }
            #[inline(always)]
            fn LN_2() -> Self { $f::consts::LN_2 as $t  }
            #[inline(always)]
            fn LOG10_E() -> Self { $f::consts::LOG10_E as $t  }
            #[inline(always)]
            fn LOG2_E() -> Self { $f::consts::LOG2_E as $t  }
            #[inline(always)]
            fn PI() -> Self { $f::consts::PI as $t  }
            #[inline(always)]
            fn SQRT_2() -> Self { $f::consts::SQRT_2 as $t  }
        }
    );
}

macro_rules! trait_const_float {
    ($t:ident) => (
        impl NumConst for $t {
            #[inline(always)]
            fn E() -> Self { $t::consts::E }
            #[inline(always)]
            fn FRAC_1_PI() -> Self { $t::consts::FRAC_1_PI }
            #[inline(always)]
            fn FRAC_1_SQRT_2() -> Self { $t::consts::FRAC_1_SQRT_2 }
            #[inline(always)]
            fn FRAC_2_PI() -> Self { $t::consts::FRAC_2_PI }
            #[inline(always)]
            fn FRAC_2_SQRT_PI() -> Self { $t::consts::FRAC_2_SQRT_PI }
            #[inline(always)]
            fn FRAC_PI_2() -> Self { $t::consts::FRAC_PI_2 }
            #[inline(always)]
            fn FRAC_PI_3() -> Self { $t::consts::FRAC_PI_3 }
            #[inline(always)]
            fn FRAC_PI_4() -> Self { $t::consts::FRAC_PI_4 }
            #[inline(always)]
            fn FRAC_PI_6() -> Self { $t::consts::FRAC_PI_6 }
            #[inline(always)]
            fn FRAC_PI_8() -> Self { $t::consts::FRAC_PI_8 }
            #[inline(always)]
            fn LN_10() -> Self { $t::consts::LN_10 }
            #[inline(always)]
            fn LN_2() -> Self { $t::consts::LN_2 }
            #[inline(always)]
            fn LOG10_E() -> Self { $t::consts::LOG10_E }
            #[inline(always)]
            fn LOG2_E() -> Self { $t::consts::LOG2_E }
            #[inline(always)]
            fn PI() -> Self { $t::consts::PI }
            #[inline(always)]
            fn SQRT_2() -> Self { $t::consts::SQRT_2 }
        }
    );
}

trait_const_fixed!(f64, usize);
trait_const_fixed!(f32, u8);
trait_const_fixed!(f32, u16);
trait_const_fixed!(f64, u32);
trait_const_fixed!(f64, u64);
trait_const_fixed!(f64, u128);

trait_const_fixed!(f64, isize);
trait_const_fixed!(f32, i8);
trait_const_fixed!(f32, i16);
trait_const_fixed!(f64, i32);
trait_const_fixed!(f64, i64);
trait_const_fixed!(f64, i128);

trait_const_float!(f32);
trait_const_float!(f64);

impl<T> NumConst for Wrapping<T>
where
    T: NumConst,
{
    #[inline(always)]
    fn E() -> Self {
        Wrapping(NumConst::E())
    }
    #[inline(always)]
    fn FRAC_1_PI() -> Self {
        Wrapping(NumConst::FRAC_1_PI())
    }
    #[inline(always)]
    fn FRAC_1_SQRT_2() -> Self {
        Wrapping(NumConst::FRAC_1_SQRT_2())
    }
    #[inline(always)]
    fn FRAC_2_PI() -> Self {
        Wrapping(NumConst::FRAC_2_PI())
    }
    #[inline(always)]
    fn FRAC_2_SQRT_PI() -> Self {
        Wrapping(NumConst::FRAC_2_SQRT_PI())
    }
    #[inline(always)]
    fn FRAC_PI_2() -> Self {
        Wrapping(NumConst::FRAC_PI_2())
    }
    #[inline(always)]
    fn FRAC_PI_3() -> Self {
        Wrapping(NumConst::FRAC_PI_3())
    }
    #[inline(always)]
    fn FRAC_PI_4() -> Self {
        Wrapping(NumConst::FRAC_PI_4())
    }
    #[inline(always)]
    fn FRAC_PI_6() -> Self {
        Wrapping(NumConst::FRAC_PI_6())
    }
    #[inline(always)]
    fn FRAC_PI_8() -> Self {
        Wrapping(NumConst::FRAC_PI_8())
    }
    #[inline(always)]
    fn LN_10() -> Self {
        Wrapping(NumConst::LN_10())
    }
    #[inline(always)]
    fn LN_2() -> Self {
        Wrapping(NumConst::LN_2())
    }
    #[inline(always)]
    fn LOG10_E() -> Self {
        Wrapping(NumConst::LOG10_E())
    }
    #[inline(always)]
    fn LOG2_E() -> Self {
        Wrapping(NumConst::LOG2_E())
    }
    #[inline(always)]
    fn PI() -> Self {
        Wrapping(NumConst::PI())
    }
    #[inline(always)]
    fn SQRT_2() -> Self {
        Wrapping(NumConst::SQRT_2())
    }
}

#[cfg(test)]
mod test {
    use core::{f32, f64};
    use super::super::ApproxEq;
    use super::NumConst;

    fn pi<T: NumConst>() -> T {
        T::PI()
    }

    #[test]
    fn test_pi() {
        assert_eq!(pi::<usize>(), 3);
        assert_eq!(pi::<isize>(), 3);
        assert!(pi::<f32>().approx_eq(&f32::consts::PI));
        assert!(pi::<f64>().approx_eq(&f64::consts::PI));
    }
}
