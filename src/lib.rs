#![feature(alloc)]
#![feature(core_intrinsics)]
#![feature(intrinsics)]
#![feature(u128_type, i128_type, u128, i128)]
#![no_std]

extern crate alloc;
extern crate libc;

mod approx_eq;
mod bounded;
mod cast;
mod float;
mod from_primitive;
mod integer;
mod num_const;
mod num;
mod one;
mod pow;
mod round;
mod signed;
mod sqrt;
mod to_primitive;
mod trig_f32;
mod trig_f64;
mod trig;
mod unsigned;
mod zero;

pub use self::trig_f32::{acosf32, acoshf32, asinf32, asinhf32, atan2f32, atanf32, atanhf32,
                         coshf32, sinhf32, tanf32, tanhf32};
pub use self::trig_f64::{acosf64, acoshf64, asinf64, asinhf64, atan2f64, atanf64, atanhf64,
                         coshf64, sinhf64, tanf64, tanhf64};

pub use self::approx_eq::ApproxEq;
pub use self::bounded::Bounded;
pub use self::cast::Cast;
pub use self::float::Float;
pub use self::from_primitive::FromPrimitive;
pub use self::integer::Integer;
pub use self::num_const::NumConst;
pub use self::num::Num;
pub use self::one::One;
pub use self::pow::Pow;
pub use self::round::Round;
pub use self::signed::Signed;
pub use self::sqrt::Sqrt;
pub use self::to_primitive::ToPrimitive;
pub use self::trig::{TO_DEGREES_F32, TO_DEGREES_F64, TO_RADS_F32, TO_RADS_F64, Trig};
pub use self::unsigned::Unsigned;
pub use self::zero::Zero;
