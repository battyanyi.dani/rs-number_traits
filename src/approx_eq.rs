use core::num::Wrapping;
use core::{f32, f64};

pub trait ApproxEq {
    #[inline]
    fn approx_eq(&self, other: &Self) -> bool;

    #[inline]
    fn approx_ne(&self, other: &Self) -> bool {
        !self.approx_eq(other)
    }

    #[inline]
    fn approx_eq_tolerance(&self, other: &Self, tolerance: &Self) -> bool;
    #[inline]
    fn approx_ne_tolerance(&self, other: &Self, tolerance: &Self) -> bool {
        !self.approx_eq_tolerance(other, tolerance)
    }
}

macro_rules! trait_approx {
    ($($T:ident),*) => (
        $(impl ApproxEq for $T {
            #[inline]
            fn approx_eq(&self, other: &Self) -> bool {
                self == other
            }
            #[inline]
            fn approx_eq_tolerance(&self, other: &Self, tolerance: &Self) -> bool {
                if self > other {
                    (self.clone() - other.clone()) < *tolerance
                } else {
                    (other.clone() - self.clone()) < *tolerance
                }
            }
        })*
    );
}

trait_approx!(
    usize,
    u8,
    u16,
    u32,
    u64,
    u128,
    isize,
    i8,
    i16,
    i32,
    i64,
    i128
);

impl ApproxEq for f32 {
    #[inline(always)]
    fn approx_eq(&self, other: &Self) -> bool {
        self.approx_eq_tolerance(other, &f32::EPSILON)
    }
    #[inline]
    fn approx_eq_tolerance(&self, other: &Self, tolerance: &Self) -> bool {
        if self > other {
            (self.clone() - other.clone()) < *tolerance
        } else {
            (other.clone() - self.clone()) < *tolerance
        }
    }
}
impl ApproxEq for f64 {
    #[inline(always)]
    fn approx_eq(&self, other: &Self) -> bool {
        self.approx_eq_tolerance(other, &f64::EPSILON)
    }
    #[inline]
    fn approx_eq_tolerance(&self, other: &Self, tolerance: &Self) -> bool {
        if self > other {
            (self.clone() - other.clone()) < *tolerance
        } else {
            (other.clone() - self.clone()) < *tolerance
        }
    }
}

impl<T> ApproxEq for Wrapping<T>
where
    T: ApproxEq,
{
    #[inline(always)]
    fn approx_eq(&self, other: &Self) -> bool {
        ApproxEq::approx_eq(&self.0, &other.0)
    }
    #[inline(always)]
    fn approx_ne(&self, other: &Self) -> bool {
        ApproxEq::approx_ne(&self.0, &other.0)
    }

    #[inline(always)]
    fn approx_eq_tolerance(&self, other: &Self, tolerance: &Self) -> bool {
        ApproxEq::approx_eq_tolerance(&self.0, &other.0, &tolerance.0)
    }
    #[inline(always)]
    fn approx_ne_tolerance(&self, other: &Self, tolerance: &Self) -> bool {
        ApproxEq::approx_ne_tolerance(&self.0, &other.0, &tolerance.0)
    }
}

#[test]
fn test_approx_eq() {
    use core::f32::consts::PI;

    assert_eq!((1u32).approx_eq(&1u32), true);
    assert_eq!((1f32).approx_eq(&(PI / PI)), true);
}
