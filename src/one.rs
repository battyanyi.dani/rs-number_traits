use core::num::Wrapping;

pub trait One {
    fn one() -> Self;
    fn is_one(&self) -> bool;
}

macro_rules! impl_one {
    ($($T:ident),+) => (
        $(
            impl One for $T {
                #[inline(always)]
                fn one() -> Self { 1 }
                #[inline]
                fn is_one(&self) -> bool {
                    self == &1
                }
            }
        )+
    );
}

macro_rules! impl_one_float {
    ($($T:ident),+) => (
        $(
            impl One for $T {
                #[inline(always)]
                fn one() -> Self { 1.0 }
                #[inline]
                fn is_one(&self) -> bool {
                    self == &1.0
                }
            }
        )+
    );
}

impl_one!(u8, u16, u32, u64, usize, i8, i16, i32, i64, isize);
impl_one!(u128, i128);
impl_one_float!(f32, f64);

impl<T> One for Wrapping<T>
where
    T: One,
{
    #[inline]
    fn one() -> Self {
        Wrapping(T::one())
    }
    #[inline]
    fn is_one(&self) -> bool {
        self.0.is_one()
    }
}
